import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachineGUI {

    public static final String COUPON_CODE = "IWANTAZITAMA";

    private JPanel root;
    private JButton misoRamenButton;
    private JButton soySauceRamenButton;
    private JButton saltRamenButton;
    private JButton tonkotsuRamenButton;
    private JButton oolongTeaButton;
    private JButton azitamaButton;
    private JTextPane textPane1;
    private JTextPane textPane2;
    private JButton cancelButton;
    private JButton checkOutButton;
    private JButton couponButton;

    public FoodOrderingMachineGUI() {
        soySauceRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action("Soy-sauce Ramen", 650);
            }
        });
        saltRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action("Salt Ramen", 700);
            }
        });
        misoRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action("Miso Ramen", 720);
            }
        });
        tonkotsuRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action("Tonkotsu Ramen", 680);
            }
        });
        oolongTeaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action("Oolong Tea", 120);
            }
        });
        azitamaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action("Azitama", 50);
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancel();
            }
        });
        couponButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                coupon();
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkOut();
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        int totalAmount =0;
    }
    public void action(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            //Yes
            JOptionPane.showMessageDialog(
                    null,
                    "Order for " + food + " received. it will be served as soon as possible!");
            countPanel(food, price);
        }
    }
    public void countPanel(String food, int price) {
        String currentText = textPane1.getText();
        textPane1.setText(currentText + food + ": " + price + "Yen\n");
        int currentAmount = Integer.parseInt(textPane2.getText());
        int totalAmount = currentAmount + price;
        textPane2.setText(String.valueOf(totalAmount));
    }
    public void coupon() {
        String str = textPane1.getText();
        int checker = str.indexOf("Azitama(free)");
        if(checker==-1) {
            String code = JOptionPane.showInputDialog("Enter coupon code.", "What do you want?");
            if(code.equals(COUPON_CODE)) {
                action("Azitama(free)", 0);
            }else {
                JOptionPane.showMessageDialog(
                        null,
                        "Coupon code is incorrect.");
            }
        }else {
            JOptionPane.showMessageDialog(
                    null,
                    "Multiple coupons cannot be used.");
        }
    }
    public  void checkOut() {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to checkout?",
                "Checkout confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            //Yes
            String totalAmount = textPane2.getText();
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you. The total price is " + totalAmount + "Yen.");
            textPane1.setText("");
            textPane2.setText("0");
        }
    }
    public void cancel() {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Do you want to cancel your order?",
                "Cancel confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            //Yes
            JOptionPane.showMessageDialog(
                    null,
                    "Order cancelled.");
            textPane1.setText("");
            textPane2.setText("0");
        }
    }
}
